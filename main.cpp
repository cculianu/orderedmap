#include "limitedmap.h"
#include "OrderedMap.h"

#include <algorithm>
#include <chrono>
#include <functional>
#include <iomanip>
#include <iostream>
#include <limits>
#include <random>
#include <ratio>
#include <string>
#include <string_view>
#include <utility>
#include <vector>

// below headers are for getN*Processors, etc.
#if defined(OS_DARWIN)
#  include <sys/types.h>
#  include <sys/sysctl.h>
#  include <mach/mach.h>
#  include <mach/mach_time.h>
#elif defined(OS_LINUX)
#  include <array>
#  include <fstream>
#  include <sstream>
#  include <strings.h>
#  include <time.h>
#  include <unistd.h>
#elif defined(OS_WINDOWS)
#define WIN32_LEAN_AND_MEAN 1
#  include <windows.h>
#  include <psapi.h>
#endif

struct MemUsage {
    std::size_t phys{}, virt{};
    static std::pair<int64_t, int64_t> diff(const MemUsage &a, const MemUsage &b) {
        return { int64_t(a.phys) - int64_t(b.phys),
                 int64_t(a.virt) - int64_t(b.virt) };
    }
};
MemUsage getProcessMemoryUsage()
{
#if defined(OS_WINDOWS)
        PROCESS_MEMORY_COUNTERS_EX pmc;
        GetProcessMemoryInfo(GetCurrentProcess(), (PROCESS_MEMORY_COUNTERS*)&pmc, sizeof(pmc));
        return { std::size_t{pmc.WorkingSetSize}, std::size_t{pmc.PrivateUsage} };
#elif defined(OS_LINUX)
        MemUsage ret;
        std::ifstream file("/proc/self/status", std::ios_base::in);
        if (!file) return ret;
        std::array<char, 256> buf;
        buf[0] = 0;
        // sizes are in kB
        while (file.getline(buf.data(), buf.size()) && (ret.phys == 0 || ret.virt == 0)) {
            if (strncasecmp(buf.data(), "VmSize:", 7) == 0) {
                std::istringstream is(buf.data() + 7);
                is >> std::skipws >> ret.virt;
                ret.virt *= std::size_t(1024);
            } else if (strncasecmp(buf.data(), "VmRSS:", 6) == 0) {
                std::istringstream is(buf.data() + 6);
                is >> std::skipws >> ret.phys;
                ret.phys *= std::size_t(1024);
            }
        }
        return ret;
#elif defined(OS_DARWIN)
        struct task_basic_info t_info;
        mach_msg_type_number_t t_info_count = TASK_BASIC_INFO_COUNT;

        if (KERN_SUCCESS != task_info(mach_task_self(), TASK_BASIC_INFO, (task_info_t)&t_info, &t_info_count)) {
            return {};
        }
        return { std::size_t{t_info.resident_size}, std::size_t{t_info.virtual_size} };
#else
        return {};
#endif
}

std::int64_t getTimeMicros()
{
    // MacOS or generic platform (on MacOS with clang this happens to be very accurate)
    static const auto t0 = std::chrono::high_resolution_clock::now();
    const auto now = std::chrono::high_resolution_clock::now();
    return std::chrono::duration_cast<std::chrono::microseconds>(now - t0).count();
}

template <typename FM>
void prt(const FM &fm) {
    std::cout << "----------" << std::endl;
    for (auto & [k, v] : fm) {
        std::cout << "{ \"" << k << "\", \"" << v << "\" }," << std::endl;
    }
    std::cout << std::endl;
}

template <>
void prt(const MemUsage &m)
{
    std::cout << "MemUsage - Phys: " << std::setprecision(2) << std::fixed << (m.phys/1e6)
              << " MB - Virt: " << std::setprecision(2) << std::fixed << (m.virt/1e6) << " MB" << std::endl;
}

template <>
void prt(const std::pair<int64_t, int64_t> &p)
{
    std::cout << "Diff - Phys: " << std::setprecision(2) << std::fixed << (p.first/1e6)
              << " MB - Virt: " << std::setprecision(2) << std::fixed << (p.second/1e6) << " MB" << std::endl;
}

template <typename FM>
void revprt(const FM &fm) {
    std::cout << "----------" << std::endl;
    for (auto it = fm.crbegin(); it != fm.crend(); ++it) {
        auto & [k, v] = *it;
        std::cout << "{ \"" << k << "\", \"" << v << "\" }," << std::endl;
    }
    std::cout << std::endl;
}

void test1()
{
    ordered_map::Map<std::string, std::string> lm;

    lm.emplace(std::piecewise_construct, std::forward_as_tuple("hi"), std::forward_as_tuple("there"));
    lm.emplace(std::piecewise_construct, std::forward_as_tuple("another"), std::forward_as_tuple("item"));
    prt(lm);

    decltype(lm) m2 = {
        { "this is a key", "this is a value" },
        { "this is another key", "this is another value" },
        { "thirdkey", "thirdval"},
        { "4thkey", "4thval" }
    };
    prt(m2);

    m2["added with operator[]"] = "yippee!";
    std::string str("hithere");
    m2[str] = "yooooo";
    prt(m2);

    m2.truncate(m2.size()-1);
    prt(m2);

    try {
        const auto &cref = m2;
        cref.at("notexist");
    } catch (const std::exception &e) {
        std::cout << "Exception: " << e.what() << std::endl;
    }
    m2.order(ordered_map::Order::RevLIFO);
    m2 = {
        { "key1" , "val1" },
        { "key2" , "val2" },
        { "key3" , "val3" },
    };
    m2.insert({"fuk", "me"});
    prt(m2);

    m2.truncate(3);
    prt(m2);

    m2.emplace_at(++m2.begin(), "emplaced_at", "yay!");
    prt(m2);

    std::cout << "Reverse iteration: " << std::endl;
    revprt(m2);

    m2.limit(5);

    m2.order(ordered_map::Order::FIFO);
    m2["pastlimit1"] = "pl1";
    m2["pastlimit2"] = "pl2";
    m2["pastlimit3"] = "pl3";

    prt(m2);

    const auto &cref = m2;
    decltype(m2) m3 = { cref.oldest(), cref.newest() };
    prt(m3);

    m3.touch("key2");
    m3.insert_or_assign(str="inserted_or_assigned", "yay!! woohoo!");
    prt(m3);
    m3.insert_or_assign(str, "modified");
    m3.insert_or_assign("tempval", "inserted2");
    m3.insert_or_assign("tempval", "inserted3");
    prt(m3);

    decltype(m3) m4{m3};
    std::cout << std::boolalpha << "Self Eq?: " << (m4 == m4) << std::endl;
    std::cout << std::boolalpha << "Eq?: " << (m4 == m3) << std::endl;
    std::cout << std::boolalpha << "NotEq?: " << (m4 != m3) << std::endl;
    m4["newitem"] = "haha";
    std::cout << std::boolalpha << "Eq?: " << (m4 == m3) << std::endl;
    std::cout << std::boolalpha << "NotEq?: " << (m4 != m3) << std::endl;

    prt(m4);
}

std::string randomString(std::size_t len)
{
    using RandByteEngine = std::independent_bits_engine<
        std::default_random_engine, 8, uint8_t>;

    static RandByteEngine rbe;
    std::string ret(len, 0);
    if (len)
        std::generate(std::begin(ret), std::end(ret), std::ref(rbe));
    return ret;
}

void testOMap(const std::size_t N, const std::size_t LIMIT)
{
    std::cout << "------------ OrderedMap ---------\n";
    prt(getProcessMemoryUsage());
    constexpr size_t KSZ = 16, VSZ = 24;
    std::vector<std::pair<std::string, std::string>> strs;
    strs.reserve(N);
    {
        auto t0 = getTimeMicros();
        std::cout << "Generating " << N << " pairs of strings... " << std::endl;
        for (size_t i = 0; i < N; ++i)
            strs.emplace_back(randomString(KSZ), randomString(VSZ));
        std::cout << "Elapsed: " << std::fixed << std::setprecision(2) << ((getTimeMicros()-t0)/1e3) << " msec\n";
    }
    const auto usage0 = getProcessMemoryUsage();
    prt(usage0);
    const auto t0 = getTimeMicros();
    ordered_map::Map<std::string, std::string> /*std::unordered_map<std::string, std::string>*/ m;
    if (LIMIT && LIMIT != ordered_map::NoLimit) {
        m.reserve(LIMIT);
        m.limit(LIMIT);
        m.max_load_factor(1.0);
    } else
        m.reserve(N);
    for (const auto & s : strs)
        m.emplace(s);
    std::cout << "Size: " << m.size() << std::endl;
    const auto tf = getTimeMicros();
    const auto usagef = getProcessMemoryUsage();
    std::cout << "Elapsed: " << ((tf-t0)/1e3) << " msec\n";
    prt(usagef);
    prt(MemUsage::diff(usagef, usage0));
    std::cout << "Ideal: " << ((N*(KSZ+VSZ))/1e6) << " MB\n";
}

void testLMap(const std::size_t N, const std::size_t LIMIT)
{
    std::cout << "------------ limitedmap ---------\n";
    prt(getProcessMemoryUsage());
    constexpr size_t KSZ = 16, VSZ = 24;
    using MapType = limitedmap<std::string, std::string>;
    std::vector<MapType::value_type> strs;
    strs.reserve(N);
    {
        auto t0 = getTimeMicros();
        std::cout << "Generating " << N << " pairs of strings... " << std::endl;
        for (size_t i = 0; i < N; ++i)
            strs.emplace_back(randomString(KSZ), randomString(VSZ));
        std::cout << "Elapsed: " << std::fixed << std::setprecision(2) << ((getTimeMicros()-t0)/1e3) << " msec\n";
    }
    const auto usage0 = getProcessMemoryUsage();
    prt(usage0);
    const auto t0 = getTimeMicros();
    MapType m(LIMIT > 0 && LIMIT != ordered_map::NoLimit ? LIMIT : 1UL);
    for (const auto & s : strs)
        m.insert(s);
    std::cout << "Size: " << m.size() << std::endl;
    const auto tf = getTimeMicros();
    const auto usagef = getProcessMemoryUsage();
    std::cout << "Elapsed: " << ((tf-t0)/1e3) << " msec\n";
    prt(usagef);
    prt(MemUsage::diff(usagef, usage0));
    std::cout << "Ideal: " << ((N*(KSZ+VSZ))/1e6) << " MB\n";
}

int main(int argc, char **argv)
{
    const auto memuse0 = getProcessMemoryUsage();

    test1();
    std::size_t N = 1'000'000, LIMIT = 0;
    bool lmap = false;

    if (argc > 1) {
        try {
            N = std::stoul(argv[1]);
            if (argc > 2)
                LIMIT = std::stoul(argv[2]);
            if (N > std::size_t(std::numeric_limits<long>::max())
                    || LIMIT > std::size_t(std::numeric_limits<long>::max()))
                throw std::out_of_range("parsed value was negative");
        } catch (const std::exception &e) {
            std::cerr << "Exception: " << e.what() << std::endl;
            return 1;
        }
    }
    if (argc > 3) // pass 3rd arg to use limitedmap
        lmap = true;

    if (!lmap)
        testOMap(N, LIMIT);
    else
        testLMap(N, LIMIT);

    std::cout << "Hit a enter to exit ... " << std::flush;
    std::cin.get();

    std::cout << "\nMemUsage after cleanup: ";
    prt(MemUsage::diff(getProcessMemoryUsage(), memuse0));

    return 0;
}
