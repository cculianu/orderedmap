#pragma once
#include <cassert>
#include <cstddef>
#include <functional>
#include <initializer_list>
#include <limits>
#include <memory>
#include <stdexcept>
#include <unordered_map>
#include <utility>

namespace ordered_map {

//! This is thrown by the Map insert(), operator[], and emplace() methods if the newly inserted item was just deleted
//! due to the fact that the limit() was exceeded and the item was reaped as a result.
//!
//! This is only ever thrown for Order::LIFO and Order::RevLIFO.
struct InsertFailure : public std::length_error {
    using std::length_error::length_error;
};

enum class Order : std::uint8_t {
    /// Items are inserted at the back and if the map is shrunk, they are popped off the front.
    FIFO,
    /// Items are inserted at the front and if the map is shrunk, they are popped off the back.
    RevFIFO,
    /// Items are inserted at the back and if the map is shrunk, they are popped off the back.
    /// Warning: Will throw in that case if limit hit on insert!
    LIFO,
    /// Items are inserted at the front and if the map is shrunk, they are popped off the front.
    /// Warning: Will throw in that case if limit hit on insert!
    RevLIFO,

    Default = FIFO,
};

//! Used by the Map::limit() facility -- this indicates no limit is in place
inline constexpr std::size_t NoLimit = std::numeric_limits<std::size_t>::max();

/// A map that preserves the order of insertions. Complexity of lookups/insertions is the same as std::unordered_map.
///
/// Optionally, this map can also enforce a size limit, whereby old items are deleted when the size limit is exceeded.
/// Uses more memory than an unordered_map, at about ~32 bytes extra per element on 64-bit CPUs.
template <typename KeyT, typename ValueT, typename HasherT = std::hash<KeyT>, typename PredT = std::equal_to<KeyT>>
class Map
{
public:
    // std container compatibility
    using size_type = std::size_t;
    using difference_type = std::ptrdiff_t;
    using key_type = KeyT;
    using mapped_type = ValueT;
    using value_type = std::pair<key_type, mapped_type>;
    using hasher = HasherT;
    using key_equal = PredT;

    using reference = value_type &;
    using const_reference = const value_type &;
    using pointer = value_type *;
    using const_pointer = const value_type *;

    using Order = ordered_map::Order;
    using InsertFailure = ordered_map::InsertFailure;

private:
    struct Node {
        value_type value;
        Node *prev = nullptr, *next = nullptr;
        template <typename ... Args>
        Node(Args && ... args) : value(std::forward<Args>(args)...) {}
        Node(const Node &) = delete;
        Node &operator=(const Node &) = delete;
    };
    // Apply hasher to *key
    struct WrapHasher {
        hasher wrappedHasher;
        std::size_t operator()(const key_type *k) const { return wrappedHasher(*k); }
    };
    // Apply key_equal to *lhs, *rhs
    struct WrapKeyEqual {
        key_equal wrappedEqFun;
        bool operator()(const key_type *lhs, const key_type *rhs) const { return wrappedEqFun(*lhs, *rhs); }
    };

    Order _order = Order::Default;
    size_type _limit = NoLimit; ///< the map will auto-delete old items if its size exceeds this
    Node *head = nullptr, *tail = nullptr;
    using HashMap = std::unordered_map<const key_type *, Node *, WrapHasher, WrapKeyEqual>;
    HashMap map;

public:
    template <typename Ptr, typename Ref, bool Reverse = false>
    class IteratorT {
        const Node *node = nullptr;
        friend class ordered_map::Map<KeyT, ValueT, HasherT>;
        explicit IteratorT(const Node *n) : node(n) {}
    public:
        IteratorT() {} // default c'tor -- itertor to end()

        IteratorT(const IteratorT &) = default;
        IteratorT(IteratorT &&) = default;

        IteratorT & operator=(const IteratorT &) = default;
        IteratorT & operator=(IteratorT &&) = default;

        Ptr operator->() const { return const_cast<Ptr>(&node->value); }
        Ref operator*() const { return const_cast<Ref>(node->value); }
        IteratorT & operator++() {
            node = Reverse ? node->prev : node->next;
            return *this;
        }
        IteratorT operator++(int) {
            IteratorT ret(*this);
            ++(*this);
            return ret;
        }

        bool operator==(const IteratorT &o) const { return node == o.node; }
        bool operator!=(const IteratorT &o) const { return !(*this == o); }
    };

    // Note: all iteraters only satisfy std::forward_iterator
    using iterator = IteratorT<pointer, reference>;
    using const_iterator = IteratorT<const_pointer, const_reference>;
    using reverse_iterator = IteratorT<pointer, reference, true>;
    using const_reverse_iterator = IteratorT<const_pointer, const_reference, true>;


    Map() = default;
    Map(size_type lim, Order ord = Order::Default) { limit(lim); order(ord); }

    Map(const Map &o) { *this = o; }
    Map(Map &&o) { *this = std::move(o); }
    Map(std::initializer_list<value_type> init) { insert(init); }

    ~Map() { clear(); }

    /// Determines how the items are inserted (and reaped if limit is hit)
    Order order() const { return _order; }
    void order(Order o) { _order = o; }

    //! Set the limit to `l`, return the number of items deleted.
    //! Note that the special value NoLimit indicates no limit should be applied.
    size_type limit(size_type l) {
        _limit = l;
        return applyLimit().first;
    }
    //! Query the limit
    size_type limit() const { return _limit; }

    iterator begin() { return iterator(head); }
    iterator end() { return iterator(); }
    const_iterator cbegin() const { return const_iterator(head); }
    const_iterator cend() const { return const_iterator(); }
    const_iterator begin() const { return cbegin(); }
    const_iterator end() const { return cend(); }
    reverse_iterator rbegin() { return reverse_iterator(tail); }
    reverse_iterator rend() { return reverse_iterator(); }
    const_reverse_iterator crbegin() const { return const_reverse_iterator(tail); }
    const_reverse_iterator crend() const { return const_reverse_iterator(); }

    iterator find(const key_type &k) {
        const auto it = map.find(&k);
        if (it != map.end())
            return iterator(it->second);
        return end();
    }

    const_iterator find(const key_type &k) const {
        const auto it = map.find(&k);
        if (it != map.end())
            return const_iterator(it->second);
        return end();
    }

    size_type size() const { return map.size(); }
    bool empty() const { return map.empty(); }
    //! Ensure .size() <= newsize.  If .size() was > newsize, then we delete the elements at the end.
    size_type truncate(size_type newsize) { return truncateImpl(newsize, nullptr).first; }

    //! deletes all elements
    void clear() { truncate(0); }

    //! rehash / reserve
    void rehash(size_type n) { map.rehash(n); }
    void reserve(size_type n) { map.reserve(n); }

    //! misc. methods of unordered_map
    float load_factor() const { return map.load_factor(); }
    float max_load_factor() const { return map.max_load_factor(); }
    void max_load_factor(float f) { map.max_load_factor(f); }
    size_type bucket_count() const { return map.bucket_count(); }
    size_type bucket_size(size_type n) const { return map.bucket_size(n); }
    size_type max_bucket_count() const { return map.max_bucket_count(); }

    size_type erase(const key_type &k) {
        auto it = map.find(k);
        if (it != map.end()) {
            Node * const delme = it->second;
            spliceOutNode(delme);
            std::unique_ptr<Node> deleter(delme); // will delete `delme` on scope end
            const auto oldsz [[maybe_unused]] = map.size();
            map.erase(it);
            assert(oldsz - map.size() == 1);
            return 1;
        }
        return 0;
    }

    iterator erase(iterator pos) {
        iterator ret;
        if (pos->node) {
            auto it = map.find(&pos->node->value.first);
            if (it != map.end()) {
                Node * const delme = it->second;
                ret.node = spliceOutNode(delme);
                std::unique_ptr<Node> deleter(delme); // will delete `delme` on scope end
                const auto oldsz [[maybe_unused]] = map.size();
                map.erase(it);
                assert(oldsz - map.size() == 1);
            } else
                // maybe pos doesn't belong to this map?
                assert(!"Unexpected: expected to find an entry in map for node, but none found in ordered_map::Map::erase()");
        }
        return ret;
    }

    //! erase in the range [first, last). First must be before last and both must be valid in *this.
    iterator erase(iterator first, iterator last) {
        while (first != last) {
            first = erase(first);
        }
        return first;
    }

    //! inserts (at the front), returns the iterator,bool, similar to std::unordered_map's insert()
    std::pair<iterator, bool> insert(const value_type &vt) {
        auto && [it, ok] = tryInsertNode(std::make_unique<Node>(vt));
        return {iterator{it->second}, ok};
    }

    //! inserts (at the front), returns the iterator,bool, similar to std::unordered_map's insert()
    std::pair<iterator, bool> insert(value_type &&vt) {
        auto && [it, ok] = tryInsertNode(std::make_unique<Node>(std::move(vt)));
        return {iterator{it->second}, ok};
    }

    template< typename P >
    std::pair<iterator, bool> insert( P && value ) { return emplace(std::forward<P>(value)); }

    template< typename... Args >
    std::pair<iterator, bool> emplace( Args &&... args ) {
        auto && [it, ok] = tryInsertNode(std::make_unique<Node>(std::forward<Args>(args)...));
        return {iterator{it->second}, ok};
    }

    //! Insert an item before the item at position pos
    template< typename... Args >
    std::pair<iterator, bool> emplace_at( iterator pos, Args &&... args ) {
        auto && [it, ok] = tryInsertNode(std::make_unique<Node>(std::forward<Args>(args)...), const_cast<Node *>(pos.node));
        return {iterator{it->second}, ok};
    }

    void insert(std::initializer_list<value_type> init) {
        for (const auto &val : init) {
            insert(val);
        }
    }

    mapped_type & at(const key_type & key) {
        auto it = map.find(&key);
        if (it != map.end())
            return it->second->value.second;
        throw std::out_of_range("key not found");
    }
    const mapped_type & at(const key_type & key) const {
        using SelfT = std::remove_cv_t<std::remove_pointer_t<decltype(this)>>;
        auto *self = const_cast<SelfT *>(this);
        return self->at(key);
    }

    mapped_type & operator[](const key_type &k) {
        auto it = map.find(&k);
        if (it != map.end()) {
            return it->second->value.second;
        }
        auto && [it2, ok] = emplace(k, mapped_type{});
        assert(ok);
        return it2->second;
    }

    mapped_type & operator[](key_type &&k) {
        auto it = map.find(&k);
        if (it != map.end()) {
            return it->second->value.second;
        }
        auto && [it2, ok] = emplace(std::move(k), mapped_type{});
        assert(ok);
        return it2->second;
    }

    template <typename M>
    std::pair<iterator, bool> insert_or_assign(const key_type & k, M && obj) {
        auto it = map.find(&k);
        if (it != map.end()) {
            it->second->value.second = std::forward<M>(obj);
            return {iterator{it->second}, false};
        } else {
            return emplace(k, std::forward<M>(obj));
        }
    }
    template <typename M>
    std::pair<iterator, bool> insert_or_assign(key_type && k, M && obj) {
        auto it = map.find(&k);
        if (it != map.end()) {
            it->second->value.second = std::forward<M>(obj);
            return {iterator{it->second}, false};
        } else {
            return emplace(std::move(k), std::forward<M>(obj));
        }
    }

    //! Return the first element in the ordered map. Note that if this map is empty(), this will segfault.
    reference front() { assert(head); return head->value; }
    const_reference front() const { assert(head); return head->value; }
    //! Return the last element in the ordered map. Note that if this map is empty(), this will segfault.
    reference back() { assert(tail); return tail->value; }
    const_reference back() const { assert(tail); return tail->value; }

    //! Returns the most recently inserted item.  This may be front() or back() depending on order.
    //! Will segfault if the map is empty().
    reference newest() {
        assert(!empty());
        switch (order()) {
        case Order::RevFIFO:
        case Order::RevLIFO:
            return front(); // canonical position at front
        case Order::FIFO:
        case Order::LIFO:
            return back();  // canonical position at back
        }
        assert(!"This should not normally be reached -- unhandled enum value!");
    }
    const_reference newest() const {
        using SelfT = std::remove_cv_t<std::remove_pointer_t<decltype(this)>>;
        auto *self = const_cast<SelfT *>(this);
        return const_cast<const_reference>(self->newest());
    }

    //! Returns the least recently inserted item.  This may be front() or back() depending on order.
    //! Will segfault if the map is empty().
    reference oldest() {
        assert(!empty());
        switch (order()) {
        case Order::RevFIFO:
        case Order::RevLIFO:
            return back();   // oldest is at back
        case Order::FIFO:
        case Order::LIFO:
            return front();  // oldest is at front
        }
        assert(!"This should not normally be reached -- unhandled enum value!");
    }
    const_reference oldest() const {
        using SelfT = std::remove_cv_t<std::remove_pointer_t<decltype(this)>>;
        auto *self = const_cast<SelfT *>(this);
        return const_cast<const_reference>(self->oldest());
    }

    //! Refreshes the key, by moving it to the canonical front/back (depending on order())
    //! In effect, this puts the key in the position it would be in if it were just freshly inserted.
    //! Returns true on success, or false if key is not found / doesn't exist.
    bool touch(const key_type &k) {
        auto it = map.find(&k);
        if (it == map.end())
            return false;
        touch(it->second);
        return true;
    }

    bool contains(const key_type &k) const { return count(k); }
    size_type count(const key_type &k) const { return map.count(&k); }

    Map & operator=(const Map &o) {
        clear();
        _order = Order::FIFO; // while we copy the map we need to enforce FIFO order
        _limit = o._limit;
        for (const auto & [k, v] : o) {
            // copy map 1 item at a time
            emplace(k, v);
        }
        _order = o._order; // finally set the real order for future inserts
        return *this;
    }

    Map & operator=(Map &&o) {
        clear();
        map = std::move(o.map);
        o.map.clear();
        head = o.head;
        tail = o.tail;
        _order = o._order;
        _limit = o._limit;
        o.head = o.tail = nullptr;
        o._order = Order::Default;
        o._limit = NoLimit;
        return *this;
    }

    Map & operator=(std::initializer_list<value_type> init) {
        clear();
        insert(init);
        return *this;
    }

    //! Element-wise operator==.  Note that order() and limit() are not considered for equality.
    bool operator==(const Map &o) const {
        if (this == &o) return true; // short-circuit if same instance
        if (size() != o.size()) return false;
        const auto &keyEq = map.key_eq();
        for (auto it_me = begin(), it_o = o.begin(); it_me != end() && it_o != o.end(); ++it_me, ++it_o)
            if (!keyEq(&it_me->first, &it_o->first) || it_me->second != it_o->second)
                return false;
        return true;
    }

    bool operator!=(const Map &o) const { return !(*this == o); }

private:
    void addNodeAtCanonicalPosition(Node *n) noexcept {
        switch (order()) {
        case Order::RevFIFO:
        case Order::RevLIFO:
            pushFront(n); // canonical position at front
            return;;
        case Order::FIFO:
        case Order::LIFO:
            pushBack(n);  // canonical position at back
            return;
        }
        assert(!"This should not normally be reached -- unhandled enum value!");
    }

    auto tryInsertNode(std::unique_ptr<Node> nptr, Node *pos = nullptr) -> std::pair<typename HashMap::iterator, bool> {
        Node * const n = nptr.get();
        auto ret = map.emplace(&n->value.first, n); // may theoretically throw, if it does, nptr will delete `n` for us
        if (ret.second) {
            if (!pos) {
                // insert in the canonical position depending on order
                addNodeAtCanonicalPosition(n);
            } else {
                // insert before `pos`
                n->next = pos;
                n->prev = pos->prev;
                pos->prev = n;
                if (n->prev) n->prev->next = n;
                if (pos == head) // if pos was head, point head to this node
                    head = n;
            }
            nptr.release(); // pointer now owned by internal linked list, release unique_ptr
            if ( applyLimit(n).second )
                // uh-oh -- this node was just inserted then deleted, indicate this with an exception
                // since the node is now gone and it's not clear *what* we can return to the caller!
                throw InsertFailure("limit exceeded, newly inserted node was immediately deleted");
        } // else -> nptr auto-deletes `n` for us
        return ret;
    }

    // returns the node following the spliced out node. may return nullptr if was last (tail) node
    Node *spliceOutNode(Node *node) noexcept {
        Node *ret{};
        if (!node)
            return ret;
        bool wasHead [[maybe_unused]] {}, wasTail [[maybe_unused]] {};
        if ((wasHead = (node == head)))
            head = node->next; // was it head? make new head be old head + 1
        if ((wasTail = (node == tail)))
            tail = node->prev; // was it tail? make new tail be old tail - 1
        if (node->prev)
            node->prev->next = node->next;
        else
            assert(wasHead); // node must have been the old head if it has no ->prev
        if (node->next) {
            ret = node->next;
            node->next->prev = node->prev;
        } else
            assert(wasTail); // node must have been the old tail if it has no ->next
        node->prev = nullptr;
        node->next = nullptr;
        assert(!head || !head->prev);
        assert(!tail || !tail->next);
        return ret;
    }
    void pushFront(Node *node) noexcept {
        if (!node) return;
        node->next = head;
        node->prev = nullptr;
        if (head)
            head->prev = node;
        head = node;
        if (!tail)
            tail = head;
    }
    void pushBack(Node *node) noexcept {
        if (!node) return;
        node->prev = tail;
        node->next = nullptr;
        if (tail)
            tail->next = node;
        tail = node;
        if (!head)
            head = tail;
    }
    auto truncateImpl(size_type newsize, Node *detectDel = nullptr) -> std::pair<size_type, bool> {
        std::pair<size_type, bool> ret{0, false};
        while (size() > newsize) {
            Node *delme{};
            switch (order()) {
            case Order::LIFO:
            case Order::RevFIFO:
                delme = tail;
                break;
            case Order::FIFO:
            case Order::RevLIFO:
                delme = head;
                break;
            }
            assert(delme);
            if (detectDel == delme)
                ret.second = true;
            spliceOutNode(delme);
            std::unique_ptr<Node> deleter(delme); // will delete `delme` on scope end
            const auto nrem [[maybe_unused]] = map.erase(&delme->value.first); // remove entry from map
            assert(nrem == 1);
            ++ret.first;
        }
        return ret;
    }
    auto applyLimit(Node *detectDel = nullptr) -> std::pair<size_type, bool> {
        if (limit() >= size())
            return {0, false};
        const auto ret = truncateImpl(limit(), detectDel);
        if (ret.first && load_factor() * 2.f < max_load_factor())
            // we shrank it and load_factor is now very low, rehash to save memory
            rehash(limit());
        return ret;
    }
    //! Take the node and puts it at the back (or front) depending on Order -- basically refreshes
    //! the node as if it were just inserted.
    void touch(Node *n) noexcept {
        if (!n) return;
        spliceOutNode(n);
        addNodeAtCanonicalPosition(n);
    }
};

} // namespace ordered_map
